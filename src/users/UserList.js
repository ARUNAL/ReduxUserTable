import React from 'react'
import  { connect } from 'react-redux'
import UserListItem from './UserListItem';
import NewUserForm from './NewUserForm'
import { removeUser } from './actions';
function userList({ users=[], onRemovePressed }) {
    return (
        <div className ="List-wrapper">
            <NewUserForm/>
            {users.map(user => <UserListItem user={user} onRemovePressed={onRemovePressed}/>)}
            
        </div>
    )
}

const mapstateToProps = state => ({
    users: state.users,
});

const mapDispatchToProps = dispatch => ({
    onRemovePressed: text => dispatch(removeUser(text))

});
export default connect(mapstateToProps,mapDispatchToProps)(userList)
