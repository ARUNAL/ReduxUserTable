import { CREATE_USER, REMOVE_USER } from './actions'
export const users =(state=[], action)=>{
    const {type, payload} = action;

    switch (type){
         case CREATE_USER : {

            const { text,number } = payload;
            const newUser ={
                text,
                number,
                isCompleted:false,
            }
            return state.concat(newUser)

         }
         case REMOVE_USER:{
             const { text,number }=payload;
             return state.filter(ur => ur.text !==text || ur.number !==number);
         }
         default:
             return state;
    }
}