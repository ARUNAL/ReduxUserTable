import React, {useState} from 'react'
import 'antd/dist/antd.css'
import { connect } from 'react-redux'
import { createUser } from './actions';
import { Form, Input,Button } from 'antd'
import { Formik } from "formik";

 function NewUserForm({ users, oncreatePressed }) {
    const [inputValue,setInputValue] = useState('')
    

    return (
        <div>
            <Formik
              initialValues={{ text: "", number: 20}}
              render={()=> (
                <Form
                value ={inputValue}
            onChange={e => setInputValue(e.target.value)} >

<p>Name:<Input style={{width: '200px',
height:'40px'}} 
            placeholder="name "
            type="text"
            /></p>
            <p>Mobile:<Input style={{width: '200px',
height:'40px'}}
            placeholder=" number "
            type="number"
            />
            
</p>
            <p><Button type="dashed"
            onClick={()=>{
                const isDupliacteText =
                users.some(user => user.text ===inputValue);
                if (!isDupliacteText){

                oncreatePressed(inputValue)
                setInputValue('')
            }
            }}> Create</Button></p>
            </Form>
  )}
/>
        </div>
    )
}
const mapstateToProps = state => ({
    users: state.users,
});

const mapDispatchToProps = dispatch => ({
    oncreatePressed: text => dispatch(createUser(text))

});

export default connect(mapstateToProps,mapDispatchToProps)(NewUserForm);
