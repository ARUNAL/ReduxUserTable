export const CREATE_USER = 'CREATE_USER'
export const createUser = (text,number) => ({
    type: CREATE_USER,
    payload: { text,number },
});

export const REMOVE_USER = 'REMOVE_USER'
export const removeUser = (text,number) => ({
    type: REMOVE_USER,
    payload: { text,number },
});