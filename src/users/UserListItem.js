import React from 'react'
import 'antd/dist/antd.css'
import { Button } from 'antd'

export default function UserListItem({ user, onRemovePressed }) {
    return (
        <div className="user-item-container">
            <h3>{user.text}</h3>
            <h3>{user.number}</h3>
            {/* <Button type="primary">mark as  completed</Button> */}
            <Button
            onClick={()=> onRemovePressed(user.text)}
            type="text" danger>Remove</Button>
            
        </div>
    )
}
