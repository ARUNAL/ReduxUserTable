import { createStore, combineReducers } from 'redux'
import { users } from './users/reducers'
const reducers ={
    users,
};

const rootReducer = combineReducers(reducers);

export const configureStore =() => createStore(rootReducer)